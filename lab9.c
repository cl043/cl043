#include <stdio.h>

void input(int *a, int *b)
{
    printf("Enter  A Number: ");
    scanf("%d",&*a);
    printf("Enter Another Number: ");
    scanf("%d",&*b);
}

void compute(int a, int b,int *sum, int *dif, int *mul, int *div, int *rem)
{
    *sum= (a)+(b);
    *dif= (a)-(b);
    *mul= (a)*(b);
    *div= (a)/(b);
    *rem= (a)%(b);
}

void output (int a, int b,int sum, int dif, int mul, int div, int rem)
{
    printf("(%d) + (%d) = %d\n",a,b,sum);
    printf("(%d) - (%d) = %d\n",a,b,dif);
    printf("(%d) * (%d) = %d\n",a,b,mul);
    printf("(%d) / (%d) = %d\n",a,b,div);
    printf("(%d) % (%d) = %d\n",a,b,rem);
}

int main ()
{
    int a,b,sum,dif,mul,div,rem;
    input(&a,&b);
    compute(a,b,&sum,&dif,&mul,&div,&rem);
    output(a,b,sum,dif,mul,div,rem);
    return 0;
}

