 
#include<stdio.h>

int input()
{
  int n;
  printf("Enter a number\n");
  scanf("%d",&n);
  return n;
}

void display(int n)
{
	printf("The multiples of %d are:\n",n);
}

void compute ( int n)
{
	int i,m;
  for(i=1;i<=100;i++)
  {
  	m=n*i;
  	printf("%d*%d=%d\n",n,i,m);
  }
}

int main()
{
	int a;
	a=input();
	display(a);
	compute(a);
	return 0;
}
