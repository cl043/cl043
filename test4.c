#include<stdio.h>

int input()
{
	int n;
	printf("Enter a number\n");
	scanf("%d",&n);
	return n;
}

int compute(int n)
{
	int sum=0,rem;
	while(n!=0)
	{
		rem=n%10;
		sum=sum+rem;
		n=n/10;
	}
	return sum;
}
 
void output (int sum)
{
	printf("Sum of the digits in the given number is %d\n",sum);
}

int main()
{
	int a,b;
	a=input();
	b=compute(a);
	output(b);
	return 0;
}
	 
