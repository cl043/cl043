#include <stdio.h>
int input ()
{
    char c;
    printf("Enter the alphabet\n");
    scanf(" %c",&c);
    return c;
}

void compute (char c)
{
    switch(c)
    {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
        case 'A':
        case 'E':
        case 'I':
        case 'O':
        case 'U':
            printf("The given alphabet is a vowel\n");
            break;
        default: 
            printf("The given alphabet isn't a vowel\n");
    }
}

int main()
{
    char c;
    c=input();
    compute(c);
    return 0;
}

