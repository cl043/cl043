#include <stdio.h>

int input()
{
    int n;
    printf("Enter the number of elements to be added:");
    scanf("%d",&n); 
    return n;
}

int compute(int n)
{
    int a,sum=0,i=0;
    
    while(i<n)
    {
        printf("Enter number %d:",i+1);
        scanf("%d",&a);
        sum=sum+a;
        i++;
    }
    return sum;
}

void output (int n,int sum)
{
    printf("Sum of the given %d numbers is %d",n,sum);
}

void main ()
{
    int n,sum;
    n=input();
    sum=compute(n);
    output(n,sum);
    
}

