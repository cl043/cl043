#include<stdio.h>

void input(char *ch)
{
   printf("\nEnter A Character In Lower Case: ");
   scanf(" %c",&*ch);
}

void convert(char *ch)
{
    *ch=*ch-32;
}

void print(char ch)
{
    printf("In Upper Case: %c\n",ch);
}

int main()
{
    char a;
    input(&a);
    convert(&a);
    print(a);
    return 0;
}