#include<stdio.h>

struct stud
{
    char name[20];
    int rn;
    char sec[5];
    char dept[5];
};
typedef struct stud Student;

void main()
{
    Student a[10];
    int i;
    printf("Enter The Student Details\n");
    for(i=0;i<10;i++)
    {
        printf("\nEnter Student %d Details:\n",i+1);
        printf("Enter Name: ");
        scanf("%s",&a[i].name);
        printf("Enter Roll No.: ");
        scanf("%d",&a[i].rn);
        printf("Enter Section: ");
        scanf("%s",&a[i].sec);
        printf("Enter Department: ");
        scanf("%s",&a[i].dept);
    }
    for(i=0;i<10;i++)
    {
        printf("\nStudent %d Details:\n",i+1);
        printf("Name: %s\n",a[i].name);
        printf("Roll No.: %d\n",a[i].rn);
        printf("Section: %s\n",a[i].sec);
        printf("Department: %s\n",a[i].dept);
    }
}