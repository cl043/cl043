 
#include<stdio.h>

int input()
{
  int n;
  printf("Enter a number\n");
  scanf("%d",&n);
  return n;
}

int compute ( int n)
{
  int rev=0,rem;
  while(n!=0)
  {
	  rem=n%10;
	  rev=rev*10+rem;
	  n=n/10;
  }
  return rev;
}

int output(int n,int rev)
{
	if(n==rev)
	{
		printf("The given number is a palindrome\n");
	}
  else
  {
  	printf("The given number isn't a palindrome\n");
  }
}

int main()
{
	int a,b;
	a=input();
	b=compute(a);
	output(a,b);
	return 0;
}
