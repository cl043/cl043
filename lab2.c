#include<stdio.h>
float input()
{
	float r;
	printf("Enter the radius of the circle\n");
	scanf("%f",&r);
	return r;
}
float area (float r)
{
	float area;
	area=3.14*r*r;
	return area;
}
void output(float r, float area)
{
	printf("The area of the circle is %0.2f\n",area);
}
int main()
{
	float rad,ar;
	rad=input();
	ar=area(rad);
	output(rad,ar);
	return 0;
}
