#include<stdio.h>

void input ( int *a, int *b)
{
    printf("\nEnter A Number: ");
    scanf("%d",&*a);
    printf("Enter Another Number: ");
    scanf("%d",&*b);
}

void disp_before(int a, int b)
{
    printf("\nBefore Swapping:\n");
    printf("A= %d\tB= %d\n",a,b);
}

int swap_1(int a, int b)
{
    int c;
    c=a;
    a=b;
    b=c;
    return 0;
}

void disp_after_1(int a, int b)
{
    printf("\nAfter Swapping By Call By Value:\n");
    printf("A= %d\tB= %d\n",a,b);
}

void swap_2(int *a,int *b)
{
    int c;
    c=*a;
    *a=*b;
    *b=c;
}

void disp_after_2(int a, int b)
{
    printf("\nAfter Swapping By Call By Reference:\n");
    printf("A= %d\tB= %d\n",a,b);
}

int main()
{
    int a,b;
    input(&a,&b);
    disp_before(a,b);
    swap_1(a,b);
    disp_after_1(a,b);
    swap_2(&a,&b);
    disp_after_2(a,b);
    return 0;
}