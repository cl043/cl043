#include <stdio.h>

void input(int *a, int *b)
{
    printf("\nEnter A Number:");
    scanf("%d",&*a);
    printf("Enter Another Number: ");
    scanf("%d",&*b);
    *a=(*a>0)?*a:(-*a);
    *b=(*b>0)?*b:(-*b);
}

void compute (int a, int b, int *gcd)
{
    int i;
    for(i=1;i<=a && i<=b;i++)
    {
        if(a%i==0 && b%i==0)
        *gcd=i;
    }
}

void output(int a, int b, int gcd)
{
    printf("\nGCD of %d and %d is %d\n",a,b,gcd);
}

int main()
{
    int a,b,gcd;
    input(&a,&b);
    compute(a,b,&gcd);
    output(a,b,gcd);
    return 0;
}
